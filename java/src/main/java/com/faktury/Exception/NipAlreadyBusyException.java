package com.faktury.Exception;

public class NipAlreadyBusyException extends Exception {

    public NipAlreadyBusyException(String message) {
        super(message);
    }
}

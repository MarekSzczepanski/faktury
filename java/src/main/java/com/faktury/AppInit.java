package com.faktury;

import com.faktury.Client.Client;
import com.faktury.Client.ClientRepository;
import com.faktury.User.Authority;
import com.faktury.User.AuthorityRepository;
import com.faktury.User.User;
import com.faktury.User.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AppInit implements ApplicationListener<ContextRefreshedEvent> {
    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final PasswordEncoder passwordEncoder;
    private final ClientRepository clientRepository;

    @Autowired
    public AppInit(UserRepository userRepository, AuthorityRepository authorityRepository, PasswordEncoder passwordEncoder, ClientRepository clientRepository) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.passwordEncoder = passwordEncoder;
        this.clientRepository = clientRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        List<Authority> authorities = authorityRepository.findAll();
        if(authorities.isEmpty()){
            Authority auth = authorityRepository.save(new Authority("ROLE_ADMIN"));

            User user = userRepository.save(new User("user@aa.aa", passwordEncoder.encode("password"), "nazwa", "126", "adres", auth));
            clientRepository.save(new Client("client_name", "1234567890", "client_address", "12345678901234567890123456", user));
        }
    }
}
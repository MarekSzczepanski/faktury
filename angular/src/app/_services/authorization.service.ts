import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Token} from "./token.model";
import {AuthInterceptor} from "./auth.interceptor";

@Injectable()
export class AuthorizationService {

  constructor(private httpClient: HttpClient) {
  }

  signIn(username: string, password: string) {
    let url = 'http://localhost:8080/oauth/token?grant_type=password&username=' + username + '&password=' + encodeURIComponent(
      password);
    let headers = new HttpHeaders();
    headers = headers.append("Authorization", "Basic " + btoa("client:123"));


    return this.httpClient.post<Token>(url, null, {headers: headers});

  }

  public getToken(): string {
    let token = localStorage.getItem('token');
    if(token == null){
      return '';
    }
    return token;
  }
}


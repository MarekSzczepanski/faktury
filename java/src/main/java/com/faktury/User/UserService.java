package com.faktury.User;


import java.util.List;

public interface UserService {
    List<UserDTO> getUsers();
    User findByEmail(String email);
}
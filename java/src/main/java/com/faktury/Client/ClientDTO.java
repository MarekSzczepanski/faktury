package com.faktury.Client;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ClientDTO {
    @Size(min = 6, max = 300, message = "Nazwa powinna powinna składać się z od 6 do 300 znaków")
    private String name;
    @Pattern(regexp = "[0-9]{10}", message = "NIP powinien składać się z 10 cyfr")
    private String nip;
    @Size(min = 6, max = 300, message = "Adres powinien powinna składać się z od 6 do 300 znaków")
    private String address;
    @Pattern(regexp = "[0-9]{26}", message = "Numer konto bankowego powinien składać się z 26 cyfr")
    private String accountNumber;

    public ClientDTO() {
    }

    public ClientDTO(String name, String nip, String address, String accountNumber) {
        this.name = name;
        this.nip = nip;
        this.address = address;
        this.accountNumber = accountNumber;
    }

    public ClientDTO(Client client) {
        this.name = client.getName();
        this.nip = client.getNip();
        this.address = client.getAddress();
        this.accountNumber = client.getAccountNumber();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
}
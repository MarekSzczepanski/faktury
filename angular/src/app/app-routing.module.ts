import {NgModule} from "@angular/core";
import {ClientsComponent} from "./clients/clients.component";
import {RouterModule, Routes} from "@angular/router";
import {SigninComponent} from "./auth/sign-in/signin.component";

const routes:Routes = [
  {path: '', component: SigninComponent},
  {path: 'clients', component: ClientsComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}

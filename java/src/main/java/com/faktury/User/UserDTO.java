package com.faktury.User;

import com.faktury.Client.ClientDTO;

import java.util.ArrayList;
import java.util.List;

public class UserDTO {
    private String email;
    private String name;
    private String nip;
    private String address;
    private List<ClientDTO> clients;
    private Authority authority;

    public UserDTO() {
    }

    public UserDTO(User user) {
        this.email = user.getEmail();
        this.name = user.getName();
        this.nip = user.getNip();
        this.address = user.getAddress();
        clients = new ArrayList<>();
        user.getClients().forEach(client -> clients.add(new ClientDTO(client)));
        this.authority = user.getAuthority();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<ClientDTO> getClients() {
        return clients;
    }

    public void setClients(List<ClientDTO> clients) {
        this.clients = clients;
    }

    public Authority getAuthority() {
        return authority;
    }

    public void setAuthority(Authority authority) {
        this.authority = authority;
    }
}

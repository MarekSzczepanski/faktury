import { Component, OnInit } from '@angular/core';
import {ClientService} from "../../_services/client.service";
import {Client} from "../client.model";
import {forEach} from "@angular/router/src/utils/collection";
import {Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {
  clients: Client[] = [];

  constructor(private clientService : ClientService, private router: Router) {}

  ngOnInit() {
    this.clientService.getClients()
      .subscribe(client => {this.clients.push(...client)}
      , error1 => {console.log('error status: ' + error1)})
      ;
    console.log(this.clients);
  }

}

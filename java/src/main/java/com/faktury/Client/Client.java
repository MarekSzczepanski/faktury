package com.faktury.Client;


import com.faktury.User.User;

import javax.persistence.*;

@Entity
@Table
public class Client {

    @GeneratedValue(strategy = GenerationType.TABLE)
    @Id
    @Column
    private Long id;

    @Column
    private String name;

    @Column
    private String nip;

    @Column
    private String address;

    @Column
    private String accountNumber;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Client() {
    }

    public Client(String name, String nip, String address, String accountNumber, User user) {
        this.name = name;
        this.nip = nip;
        this.address = address;
        this.accountNumber = accountNumber;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
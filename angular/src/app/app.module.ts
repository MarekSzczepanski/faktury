import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {SigninComponent} from './auth/sign-in/signin.component';
import {ClientsComponent} from './clients/clients.component';
import {AppRoutingModule} from "./app-routing.module";
import {AuthorizationService} from "./_services/authorization.service";
import { ClientListComponent } from './clients/client-list/client-list.component';
import { ClientItemComponent } from './clients/client-list/client-item/client-item.component';
import {ClientService} from "./_services/client.service";
import {AuthInterceptor} from "./_services/auth.interceptor";


@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    ClientsComponent,
    ClientListComponent,
    ClientItemComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    AuthorizationService,
    ClientService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthorizationService} from "../../_services/authorization.service";
import {Router} from "@angular/router";
import {error} from "util";
import {Observable} from "rxjs/Observable";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
  providers: []
})
export class SigninComponent implements OnInit {
  signinForm: FormGroup;
  emailCtrl: FormControl;
  passwordCtrl: FormControl;
  errorz = 'asd';

  constructor(private authorizationService : AuthorizationService, private router:Router) { }

  ngOnInit() {
    this.emailCtrl = new FormControl('user@aa.aa', [
      Validators.required,
      Validators.email,
      Validators.minLength(6),
      Validators.maxLength(100)
    ]);

    this.passwordCtrl = new FormControl('password', [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(100)
    ]);

    this.signinForm = new FormGroup({
        email: this.emailCtrl,
        password : this.passwordCtrl
    });
  }

  onSubmit() {
    let email = this.signinForm.get('email').value;
    let password = this.signinForm.get('password').value;
    this.authorizationService.signIn(email, password)
      .subscribe(token =>{
        localStorage.setItem('token', 'bearer ' +token.access_token);
        this.router.navigate(['/clients']);
        ;
      });
  }
}


package com.faktury.Client;


import com.faktury.Exception.NipAlreadyBusyException;
import com.faktury.User.User;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService{
    private final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public List<ClientDTO> getUsersClients(User user) {
        List<Client> clients = clientRepository.findByUser(user);
        List<ClientDTO> clientsDao = new ArrayList<>(clients.size());
        clients.forEach(client -> clientsDao.add(new ClientDTO(client)));
        return clientsDao;
    }

    @Override
    public ClientDTO getUserClient(User user, String nip) throws Exception {
        Client client = clientRepository.findByUserAndNip(user, nip);
        if(client == null) {
            throw new NotFoundException("Nie znaleziono klienta");
        }
        return new ClientDTO(client);
    }

    @Override
    public Long addClient(User user, ClientDTO clientDTO) throws Exception{
        if(clientRepository.findByUserAndNip(user, clientDTO.getNip()) != null){
            throw new NipAlreadyBusyException("Istnieje klient o podanym NIPie");
        }
        Client client = new Client();
        client.setName(clientDTO.getName());
        client.setAddress(clientDTO.getAddress());
        client.setNip(clientDTO.getNip());
        client.setAccountNumber(clientDTO.getAccountNumber());
        client.setUser(user);
        client = clientRepository.save(client);
        return client.getId();
    }

    @Override
    public ClientDTO deleteClient(User user, String nip) throws Exception{
        Client client= clientRepository.findByUserAndNip(user, nip);
        if(client == null) {
            throw new NotFoundException("Nie znaleziono klienta");
        }
        clientRepository.delete(client.getId());
        return new ClientDTO(client);
    }

    @Override
    public ClientDTO updateClient(User user, String nip, ClientDTO clientDTO) throws Exception{
        Client client = clientRepository.findByUserAndNip(user, nip);
        if(client == null){
            throw new NotFoundException("Nie znaleziono klienta");
        }

        Client newClient = clientRepository.findByUserAndNip(user, clientDTO.getNip());
        if(!client.getNip().equals(clientDTO.getNip()) && newClient != null){
            throw new NipAlreadyBusyException("Istnieje klient o podanym NIPie");
        }

        client.setName(clientDTO.getName());
        client.setAddress(clientDTO.getAddress());
        client.setNip(clientDTO.getNip());
        client.setAccountNumber(clientDTO.getAccountNumber());

        clientRepository.save(client);

        return clientDTO;
    }
}
package com.faktury.Client;


import com.faktury.Exception.NipAlreadyBusyException;
import com.faktury.User.User;
import com.faktury.User.UserService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.ClientAlreadyExistsException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.security.Principal;

@RestController
@RequestMapping(value = "/clients")
public class ClientController {
    private final ClientService clientService;
    private final UserService userService;

    @Autowired
    public ClientController(ClientService clientService, UserService userService) {
        this.clientService = clientService;
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getUsersClients(Principal principal){
        User user = userService.findByEmail(principal.getName());
        return ResponseEntity.ok(clientService.getUsersClients(user));
    }

    @RequestMapping
    public ResponseEntity<?> getUserClient(Principal principal, @PathVariable String  nip) throws Exception{
        try {
            User user = userService.findByEmail(principal.getName());
            return ResponseEntity.ok(clientService.getUserClient(user, nip));
        } catch (NotFoundException ex) {
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addClient(Principal principal, @RequestBody @Valid ClientDTO clientDTO) throws Exception{
        try {
            User user = userService.findByEmail(principal.getName());
            return ResponseEntity.ok(clientService.addClient(user, clientDTO));
        } catch (ClientAlreadyExistsException ex){
            return new ResponseEntity<>(ex.getMessage(),HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "{nip}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteClient(Principal principal, @PathVariable String nip) throws Exception{
        try {
            User user = userService.findByEmail(principal.getName());
            return ResponseEntity.ok(clientService.deleteClient(user, nip));
        } catch (NotFoundException ex){
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "{nip}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateClient(Principal principal, @PathVariable String nip, @RequestBody @Valid ClientDTO clientDTO) throws Exception{
        try {
            User user = userService.findByEmail(principal.getName());
            return ResponseEntity.ok(clientService.updateClient(user, nip, clientDTO));
        } catch (NotFoundException ex){
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
        } catch (NipAlreadyBusyException ex){
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
import {Injectable} from "@angular/core";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {AuthorizationService} from "./authorization.service";
import {Router} from "@angular/router";

import 'rxjs/add/operator/catch';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthorizationService, private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token = this.authService.getToken();

    if (req.headers.has('Authorization')) {
      return next.handle(req);
    }

    const copiedReq = req.clone({headers: req.headers.set('Authorization', token)});
    return next.handle(copiedReq)
      .catch((error:  HttpErrorResponse ) => {
        if (error.status == 401) {
          this.router.navigate(['/']);
        }
        return Observable.throw(error.status);
      });
    }
}

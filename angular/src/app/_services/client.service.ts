import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {AuthorizationService} from "./authorization.service";
import {Client} from "../clients/client.model";
import 'rxjs/add/operator/map'
import {Observable} from "rxjs/Observable";

@Injectable()
export class ClientService {

  constructor(private httpClient: HttpClient,private authService: AuthorizationService) {
  }

  getClients(){
    const token = this.authService.getToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    let url = 'http://localhost:8080/clients';
    return this.httpClient.get<Client[]>(url, httpOptions);
  }

}

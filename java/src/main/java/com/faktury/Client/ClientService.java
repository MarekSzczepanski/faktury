package com.faktury.Client;

import com.faktury.User.User;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ClientService {
    List<ClientDTO> getUsersClients(User user);
    ClientDTO getUserClient(User user, String nip) throws Exception;
    Long addClient(User user, ClientDTO clientDTO) throws Exception;
    ClientDTO deleteClient(User user, String id) throws Exception;
    ClientDTO updateClient(User user, String nip, ClientDTO clientDTO) throws Exception;
}